# Creation of the Database for the website

FROM postgres:alpine

ENV POSTGRES_DB pweb

ENV POSTGRES_PASSWORD postgres

ADD api/models/schema.sql /docker-entrypoint-initdb.d/base.sql
