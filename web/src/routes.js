import index from './components/Index.vue';
import cart from './components/Cart.vue';
import category from './components/Category.vue';
import product from './components/Product.vue';
import transaction from './components/Transaction.vue';

export const routes = [
  {
    path: '/',
    component: index,
  },
  {
    path: '/cart',
    component: cart,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/transactions',
    component: transaction,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/category/:id',
    component: category,
  },
  {
    path: '/category/:cat/:id',
    component: category,
  },
  {
    path: '/products/:id',
    component: product,
    props: true,
  },
];
