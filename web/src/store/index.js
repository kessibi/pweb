import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    login: {
      jwt: '',
      cart: [],
    },
  },
  plugins: [
    createPersistedState({
      storage: {
        getItem: key => localStorage.getItem(key),
        setItem: (key, value) => localStorage.setItem(key, value),
        removeItem: key => localStorage.removeItem(key),
      },
    }),
  ],

  mutations: {
    add_n_cart: (state, data) => {
      let a = false;
      for (let i = 0; i < state.login.cart.length && !a; i++) {
        if (state.login.cart[i].id == data.elt) {
          state.login.cart[i].nb = data.nb;
          a = true;
        }
      }
    },
    add_cart: (state, elt) => {
      let a = false;
      for (let i = 0; i < state.login.cart.length && !a; i++) {
        if (state.login.cart[i].id == elt) {
          state.login.cart[i].nb++;
          a = true;
        }
      }
      if (!a) {
        let b = {};
        b.id = elt;
        b.nb = 1;
        state.login.cart.push(b);
      }
    },
    del_cart: (state, elt) => {
      let a = false;
      for (let i = 0; i < state.login.cart.length && !a; i++) {
        if (state.login.cart[i].id == elt) {
          state.login.cart.splice(i, 1);
          a = true;
        }
      }
    },
    add_jwt: (state, jwt) => {
      state.login.jwt = jwt;
    },
    del_jwt: state => {
      state.login.jwt = '';
      state.login.cart = [];
    },
  },
  getters: {
    jwt: state => state.login.jwt,
    cart: state => state.login.cart,
  },
});
