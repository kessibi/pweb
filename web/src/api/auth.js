import {get, post} from '@/api/http';

export default {
  signin(user_id, password) {
    return new Promise((resolve, reject) => {
      const login = {
        user_id: user_id,
        password: password,
      };
      post('/user/signin', login)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  signup(email, password) {
    return new Promise((resolve, reject) => {
      if (email.length == 0 || password == 0) return reject('no');

      const login = {
        email: email,
        password: password,
      };
      post('/user/signup', login)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  // the jwt is in the headers
  authenticate() {
    return new Promise((resolve, reject) => {
      get('/user/auth')
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
};
