import {get} from '@/api/http';

export default {
  getCategories() {
    return new Promise((resolve, reject) => {
      get('/categories')
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  getCountries() {
    return new Promise((resolve, reject) => {
      get('/countries')
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  getCoupon(c) {
    return new Promise((resolve, reject) => {
      get(`/coupons/${c}`)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  getSubCategories(cat) {
    return new Promise((resolve, reject) => {
      get(`/categories/${cat}`)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  getSubProducts(cat) {
    return new Promise((resolve, reject) => {
      get(`/subcategory/${cat}`)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  getProduct(id) {
    return new Promise((resolve, reject) => {
      get(`/products/${id}`)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  search(query) {
    return new Promise((resolve, reject) => {
      get(`/search/${query}`)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
};
