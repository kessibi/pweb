import {get, post} from '@/api/http';

export default {
  proceed(infos) {
    return new Promise((resolve, reject) => {
      post('/pay', infos)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
  transactions() {
    return new Promise((resolve, reject) => {
      get('/transactions', {})
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  },
};
