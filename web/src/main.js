import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import App from './App.vue';
import {routes} from './routes';
import store from './store';
import VModal from 'vue-js-modal';
import 'vuetify/dist/vuetify.min.css';
Vue.config.productionTip = false;

const router = new VueRouter({
  routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.jwt) {
      next({path: '/'});
    } else {
      next();
    }
  } else {
    next();
  }
});

Vue.use(VModal);
Vue.use(VueRouter);
Vue.use(Vuetify);

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
