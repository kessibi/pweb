-- first you have to create DB pweb;

CREATE SEQUENCE checkout_seq START WITH 1 INCREMENT BY 1;

SELECT nextval('checkout_seq');

CREATE TABLE users(
  email VARCHAR(128) not null UNIQUE PRIMARY KEY,
  password VARCHAR(64) not null
);

CREATE TABLE countries(
  id VARCHAR(3) not null UNIQUE PRIMARY KEY,
  name VARCHAR(128) not null,
  tax INTEGER
);

CREATE TABLE coupons(
  id VARCHAR(10) not null UNIQUE PRIMARY KEY,
  reduction INTEGER
);

CREATE TABLE categories(
  id VARCHAR(16) PRIMARY KEY,
  name VARCHAR(32)
);

CREATE TABLE subcategories(
  id VARCHAR(16) PRIMARY KEY,
  name VARCHAR(32),
  parent VARCHAR(16) references categories(id)
);

SET timezone = 'Europe/Paris';

CREATE TABLE products(
  id SERIAL PRIMARY KEY UNIQUE,
  category VARCHAR(16) references subcategories(id),
  name VARCHAR(128) NOT NULL,
  rating INTEGER check(rating >= 0 AND rating <=10),
  description VARCHAR(1024),
  price INTEGER,
  reduction INTEGER,
  registered TIMESTAMP
);

CREATE TABLE checkout(
  id INTEGER PRIMARY KEY UNIQUE,
  price INTEGER not null,
  user_id VARCHAR(128),
  country VARCHAR(2) references countries(id),
  region VARCHAR(64) not null,
  address VARCHAR(128) not null,
  coupon VARCHAR(10)
);

CREATE TABLE checkout_products(
  c_id INTEGER references checkout(id),
  p_id INTEGER references products(id),
  amount INTEGER
);

INSERT INTO countries VALUES('fr', 'France', 10);
INSERT INTO countries VALUES('be', 'Belgium', 13);
INSERT INTO countries VALUES('ch', 'Switzerland', 20);
INSERT INTO countries VALUES('gb', 'Great Britain', 15);

INSERT INTO categories VALUES('office', 'Office');
INSERT INTO categories VALUES('design', 'Design');
INSERT INTO categories VALUES('multimedia', 'Multimedia');
INSERT INTO categories VALUES('developer', 'Developer');
INSERT INTO categories VALUES('utilitites', 'Utilities');
INSERT INTO categories VALUES('games', 'Games');

INSERT INTO coupons VALUES('COUPON', 10);

INSERT INTO subcategories VALUES(
  'keyboards',
  'Keyboards',
  'multimedia'
);

INSERT INTO subcategories VALUES(
  'mice',
  'Mice',
  'multimedia'
);

INSERT INTO subcategories VALUES(
  'screens',
  'Screens',
  'multimedia'
);

INSERT INTO subcategories VALUES(
  'pens',
  'Pens',
  'office'
);

INSERT INTO subcategories VALUES(
  'folders',
  'File Folders',
  'office'
);

INSERT INTO subcategories VALUES(
  'scissors',
  'Scissors',
  'office'
);

INSERT INTO products(category, name, rating, description, price, reduction, registered) VALUES(
  'screens', 'LG screen', 8, 'very good screen indeed', 150, 20, now()
);
INSERT INTO products(category, name, rating, description, price, reduction, registered) VALUES(
  'screens', 'Samsung screen', 8, 'what a screen', 160, 0, now()
);
INSERT INTO products(category, name, rating, description, price, reduction, registered) VALUES(
  'screens', 'Dell screen', 9, 'Very very good one', 150, 18, now()
);
