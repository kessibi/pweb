const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');
const checkAuth = require('./checkAuth');

// Returns the list of all transactions a client has made on our website
// The client is identified with the middleware checkAuth
router.get('/', checkAuth, (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query(
    `SELECT a.id, a.address, a.region, b.name as country, a.price, a.coupon
    from checkout a
    inner join countries b
    on a.country = b.id
    where user_id = $1;`,
    [req.user_data.user],
    (err, result) => {
      if (err) {
        res.status(500).json({
          error: 'There was an error retrieving categories',
        });
      } else {
        res.status(200).json(result.rows);
      }
      pool.end();
    },
  );
});

module.exports = router;
