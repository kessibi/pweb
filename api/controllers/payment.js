const express = require('express');
const router = express.Router();
const checkAuth = require('./checkAuth');
const {Pool} = require('pg');
const connecString = require('../models/db');

router.post('/', checkAuth, async (req, res, next) => {
  // Test the post request before working with the database
  if (!req.body.country)
    return res.status(500).json({
      error: 'You need to specify a country',
    });
  if (!req.body.card)
    return res.status(500).json({
      error: 'You need to specify card informations',
    });
  if (!req.body.products)
    return res.status(500).json({
      error: 'You need to have products in your cart to checkout',
    });

  const pool = new Pool({
    connectionString: connecString,
  });

  var checkout = 0;

  await pool.query(
    'INSERT INTO checkout VALUES(nextval($7), $1, $2, $3, $4, $5, $6);',
    [
      checkout,
      req.user_data.user,
      req.body.country.id,
      req.body.card.region,
      req.body.card.address,
      req.body.coupon.id,
      'checkout_seq',
    ],
  );

  let val = await pool.query('SELECT currval($1)', ['checkout_seq']);
  val = val.rows[0].currval;

  await Promise.all(
    req.body.products.map(async p => {
      const {rows} = await pool.query('SELECT * FROM products WHERE id = $1', [
        p.id,
      ]);
      if (rows.length) {
        checkout +=
          p.nb * (rows[0].price - (rows[0].price * rows[0].reduction) / 100);

        await pool.query('INSERT INTO checkout_products VALUES($1, $2, $3);', [
          val,
          p.id,
          p.nb,
        ]);
      } else {
        return res.status(401).json({
          error: 'The product was not found in the db.',
        });
      }
    }),
  );

  //  Add country delivery fee
  const countries = await pool.query('SELECT * FROM countries where id = $1;', [
    req.body.country.id,
  ]);
  if (countries.rows.length == 0) {
    return res.status(401).json({
      error: 'The country was not found in the db.',
    });
  } else {
    checkout += countries.rows[0].tax;
  }

  // Deduct coupon if one is found
  const coupon = await pool.query('SELECT * FROM coupons where id = $1;', [
    req.body.coupon.id,
  ]);
  if (coupon.rows.length != 0) {
    checkout -= coupon.rows[0].reduction;
  }

  // Update price to matching price
  await pool.query('UPDATE checkout SET price = $1 where id = $2', [
    checkout,
    val,
  ]);

  // Tell the client that everything went smoothly
  res.status(200).json({
    msg: 'Checkout completed! Expect an email from us very soon.',
  });
});

module.exports = router;
