const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');


// Will check for a coupon based on the ID, it is purely aesthetic to reassure
// the client before a checkout
router.get('/:id', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query('SELECT * from coupons where id = $1;', [req.params.id], (err, result) => {
    if (err) {
      res.status(500).json({
        error: 'There was an error retrieving coupons',
      });
    } else if(result.rows.length == 0) {
      res.status(404).json({
        error: 'There is no such coupon',
      });
    } else {
      res.status(200).json(result.rows[0]);
    }
    pool.end();
  });
});

module.exports = router;
