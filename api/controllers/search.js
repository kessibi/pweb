const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');


// Check if the query matches a product's name in the db
// returns all the server found
router.get('/:query', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query(
    'SELECT * from products where lower(name) ~ lower($1);',
    [req.params.query],
    (err, result) => {
      if (err) {
        res.status(500).json({
          error: 'There was an error retrieving object from databaes',
        });
      } else {
        let d = {};
        d.query = req.params.query;
        d.results = result.rows;
        res.status(200).json(d);
      }
      pool.end();
    },
  );
});

module.exports = router;
