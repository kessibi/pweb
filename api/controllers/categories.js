const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');

router.get('/', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query('SELECT * from categories;', [], (err, result) => {
    if (err) {
      res.status(500).json({
        error: 'There was an error retrieving categories',
      });
    } else {
      res.status(200).json(result.rows);
    }
    pool.end();
  });
});

router.get('/:id', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  let data = {};

  pool.query(
    'SELECT id, name from subcategories where parent = $1;',
    [req.params.id],
    (err, result) => {
      if (err) {
        res.status(500).json({
          error: 'There was an error retrieving categories',
        });
      } else {
        data.sub = result.rows;

        pool.query(
          `SELECT a.* FROM products a INNER JOIN subcategories b ON
          a.category = b.id where b.parent = $1`,
          [req.params.id],
          (err, result) => {
            if (err)
              res.status(500).json({
                error: 'There was an error retrieving categories',
              });
            else {
              data.products = result.rows;
              res.status(200).json(data);
            }
            pool.end();
          },
        );
      }
    },
  );
});

module.exports = router;
