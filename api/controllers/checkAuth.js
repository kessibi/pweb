const jwt = require('jsonwebtoken');
const PASS_PHRASE = require('../PASS_PHRASE');


// Checks the token given in headers to see if it matches
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, PASS_PHRASE);
    req.user_data = decoded;
    next();
  } catch(error) {
    return res.status(401).json({
      message: 'Authentification failed'
    });
  }
};

