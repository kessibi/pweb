const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const {Pool} = require('pg');
const connecString = require('../models/db');
const checkAuth = require('./checkAuth');
const PASS_PHRASE = require('../PASS_PHRASE');

// auth: if the middleware does not fail, then it is that the token
// is a legitimate one
router.get('/auth', checkAuth, (req, res) => {
  res.status(200).json({});
});

// With the user id and password, return him a signed JWT
router.post('/signin', (req, res, next) => {
  if (!req.body.user_id)
    return res.status(500).json({
      error: 'You need to give a user id',
    });
  if (!req.body.password)
    return res.status(500).json({
      error: 'You need to give a password',
    });

  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query(
    'SELECT * FROM users WHERE email = $1;',
    [req.body.user_id],
    (err, result) => {
      if (err || result.rows.length == 0) {
        res.status(403).json({
          error: 'User not found',
        });
      } else if (
        bcrypt.compareSync(req.body.password, result.rows[0].password)
      ) {
        const token = jwt.sign(
          {
            user: req.body.user_id,
            iat: new Date().valueOf() - 30,
            exp: new Date().valueOf() + 7200,
          },
          PASS_PHRASE,
        );
        res.status(200).json({
          message: 'success',
          token: token,
        });
      } else {
        res.status(403).json({
          message: 'wrong password',
        });
      }
      pool.end();
    },
  );
});

//  Create an account and return him a JWT
router.post('/signup', (req, res, next) => {
  //  Check if everything is here
  if (!req.body.email)
    return res.status(500).json({
      error: 'You need to give a user id',
    });
  if (!req.body.password)
    return res.status(500).json({
      error: 'You need to give a password',
    });

  const pool = new Pool({
    connectionString: connecString,
  });

  const hash = bcrypt.hashSync(req.body.password);

  pool.query(
    'INSERT INTO users VALUES($1, $2);',
    [req.body.email, hash],
    (err, result) => {
      if (err) {
        res.status(403).json({
          error: 'Could not add user to database',
        });
      } else {
        const token = jwt.sign(
          {
            user: req.body.email,
            iat: new Date().valueOf() - 30,
            exp: new Date().valueOf() + 7200,
          },
          PASS_PHRASE,
        );
        res.status(201).json({token: token});
      }
      pool.end();
    },
  );
});

module.exports = router;
