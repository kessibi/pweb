const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');


// Returns the list of countries in the database or an error
router.get('/', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query('SELECT * from countries;', [], (err, result) => {
    if (err) {
      res.status(500).json({
        error: 'There was an error retrieving countries',
      });
    } else {
      res.status(200).json(result.rows);
    }
    pool.end();
  });
});

module.exports = router;
