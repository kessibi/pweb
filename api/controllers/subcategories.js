const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');

router.get('/:id', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });

  pool.query(
    `SELECT category, id, name, rating, price, reduction, registered 
    from products where category = $1;`,
    [req.params.id],
    (err, result) => {
      if (err) {
        res.status(500).json({
          error: 'There was an error retrieving categories',
        });
      } else if (!result.rows.length) {
        res.status(200).json([]);
      } else {
        res.status(200).json(result.rows);
      }
      pool.end();
    },
  );
});

module.exports = router;
