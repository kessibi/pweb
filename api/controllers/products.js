const express = require('express');
const router = express.Router();
const {Pool} = require('pg');
const connecString = require('../models/db');

// Retrieve all useful informations to give to the client
// If an error occurs or the product does not exist, tell the client
router.get('/:id', (req, res, next) => {
  const pool = new Pool({
    connectionString: connecString,
  });
  pool.query(
    'SELECT * from products where id = $1;',
    [req.params.id],
    (err, result) => {
      if (err) {
        res.status(500).json({
          error: 'There was an error retrieving object from database',
        });
      } else if (result.rows.length == 0) {
        res.status(400).json({
          error: 'The product does not exist',
        });
      } else {
        res.status(200).json(result.rows[0]);
      }
      pool.end();
    },
  );
});

module.exports = router;
