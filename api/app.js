const compression = require('compression');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// API routes
const user_routes = require('./controllers/user');
const cat_routes = require('./controllers/categories');
const subcat_routes = require('./controllers/subcategories');
const products = require('./controllers/products');
const search = require('./controllers/search');
const countries = require('./controllers/countries');
const coupons = require('./controllers/coupons');
const pay = require('./controllers/payment');
const transaction = require('./controllers/transactions');

//  gzip compression
app.use(compression());

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//  Useful to prevent CORS errors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
    return res.status(200).json({});
  }
  next();
});

app.use('/user', user_routes);
app.use('/categories', cat_routes);
app.use('/subcategory', subcat_routes);
app.use('/products', products);
app.use('/search', search);
app.use('/countries', countries);
app.use('/coupons', coupons);
app.use('/pay', pay);
app.use('/transactions', transaction);

app.get('*', (req, res, next) => {
  res.send(req.params);
});

module.exports = app;
